<?php include('header.php');?>
<!-- BEGIN SLIDER -->

<div class="page-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-slider">
        <!-- Indicators -->
        <ol class="carousel-indicators carousel-indicators-frontend">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <!-- First slide -->
            <div class="item carousel-item-eight active">
                <div class="container">
                    <div class="carousel-position-six text-uppercase text-center">
                        <h2 class="margin-bottom-20 animate-delay carousel-title-v5" data-animation="animated fadeInDown">
                            Expore the power <br/>
                            <span class="carousel-title-normal">of Metronic</span>
                        </h2>
                        <p class="carousel-subtitle-v5 border-top-bottom margin-bottom-30" data-animation="animated fadeInDown">This is what you were looking for</p>
                        <a class="carousel-btn-green" href="#" data-animation="animated fadeInUp">Purchase Now!</a>
                    </div>
                </div>
            </div>

            <!-- Second slide -->
            <div class="item carousel-item-nine">
                <div class="container">
                    <div class="carousel-position-six">
                        <h2 class="animate-delay carousel-title-v6 text-uppercase" data-animation="animated fadeInDown">
                            Need a website design?
                        </h2>
                        <p class="carousel-subtitle-v6 text-uppercase" data-animation="animated fadeInDown">
                            This is what you were looking for
                        </p>
                        <p class="carousel-subtitle-v7 margin-bottom-30" data-animation="animated fadeInDown">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/>
                            Sed est nunc, sagittis at consectetur id.
                        </p>
                        <a class="carousel-btn-green" href="#" data-animation="animated fadeInUp">Purchase Now!</a>
                    </div>
                </div>
            </div>

            <!-- Third slide -->
            <div class="item carousel-item-ten">
                <div class="container">
                    <div class="carousel-position-six">
                        <h2 class="animate-delay carousel-title-v6 text-uppercase" data-animation="animated fadeInDown">
                            Powerful &amp; Clean
                        </h2>
                        <p class="carousel-subtitle-v6 text-uppercase" data-animation="animated fadeInDown">
                            Responsive Website &amp; Admin Theme
                        </p>
                        <p class="carousel-subtitle-v7 margin-bottom-30" data-animation="animated fadeInDown">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/>
                            Sed est nunc, sagittis at consectetur id.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
</div>
<!-- END SLIDER -->

<div class="main">
    <div class="container">
<!-- BEGIN SERVICE BOX -->
    <div class="about-us">
    <p>

    </p><b>SoftSite Pvt. Ltd.</b> is a one stop shop for all your software needs, our team of highly experienced and competitive software enthusiasts has helped many reputed companies in modernizing their management systems. We design and develop software which do not only cater to organizations’ growing business needs but we also help them in enhancing their business potential.
</div>



<div class="row service-box margin-bottom-40">
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-globe blue"></i></em>
            <span>Web Developement</span>
        </div>
        <p class="page-font">
            We create sophisticated responsive websites in most economical rates to help improve your branding and empower your business.
        </p>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-desktop red"></i></em>
            <span>Desktop Developement</span>
        </div>
        <p class="page-font">
            We create sophisticated responsive websites in most economical rates to help improve your branding and empower your business.
        </p>
    </div>
    <div class="col-md-4 col-sm-4">

        <div class="service-box-heading">
            <em><i class="fa fa-mobile green"></i></em>
            <span>Mobile App Developement</span>
        </div>
        <p class="page-font">
            Over 200K hours of design and development having rolled over 150+ apps to date, we specialize in crafting Mobile apps, Mobile Games for iOS, Android and Windows smart devices.
        </p>
    </div>
</div>
<!-- END SERVICE BOX -->




<!-- BEGIN STEPS -->
<div class="row margin-bottom-40 front-steps-wrapper front-steps-count-3">
    <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step1">
            <h2>Goal definition</h2>
            <p class="page-font">Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step2">
            <h2>Analyse</h2>
            <p class="page-font">Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step3">
            <h2>Implementation</h2>
            <p class="page-font">Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
        </div>
    </div>
</div>
<!-- END STEPS -->

    <!-- BEGIN TABS AND TESTIMONIALS -->
    <div class="row mix-block margin-bottom-40">
        <!-- TABS -->
        <!-- TABS -->
        <!-- TESTIMONIALS -->
        <div class="col-md-2">

        </div>
        <div class="col-md-8 testimonials-v1">
            <div id="myCarousel" class="carousel slide">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <blockquote><p class="page-font">Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met.</p></blockquote>
                        <div class="carousel-info">
                            <img class="pull-left" src="<?php echo base_url()?>assets/pages/img/people/img1-small.jpg" alt="">
                            <div class="pull-left">
                                <span class="testimonials-name">Lina Mars</span>
                                <span class="testimonials-post">Commercial Director</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <blockquote><p class="page-font">Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p></blockquote>
                        <div class="carousel-info">
                            <img class="pull-left" src="<?php echo base_url()?>assets/pages/img/people/img5-small.jpg" alt="">
                            <div class="pull-left">
                                <span class="testimonials-name">Kate Ford</span>
                                <span class="testimonials-post">Commercial Director</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <blockquote><p class="page-font">Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p></blockquote>
                        <div class="carousel-info">
                            <img class="pull-left" src="<?php echo base_url()?>assets/pages/img/people/img2-small.jpg" alt="">
                            <div class="pull-left">
                                <span class="testimonials-name">Jake Witson</span>
                                <span class="testimonials-post">Commercial Director</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Carousel nav -->
                <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
                <a class="right-btn" href="#myCarousel" data-slide="next"></a>
            </div>
        </div>
        <div class="col-md-2">

        </div>
        <!-- END TESTIMONIALS -->
    </div>
    <!-- END TABS AND TESTIMONIALS -->
<!-- BEGIN CLIENTS -->
<div class="row margin-bottom-40 our-clients">
    <div class="col-md-3">
        <h2><a href="javascript:;">Our Clients</a></h2>
        <p class="page-font">We have many clients. Some of them are mentioned.</p>
    </div>
    <div class="col-md-9">
        <div class="owl-carousel owl-carousel6-brands">


            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/axn-logo-hover.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/axn-logo.png" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/BMC-logo-hover.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/BMC-logo-hover.png" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/naaptol-logo-hover.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/naaptol-logo.png" class="color-img img-responsive" alt="">
                </a>
            </div>

            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/leo-logo-hover.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/leo.png" class="color-img img-responsive" alt="">
                </a>
            </div>

            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/fnac-logo-hover.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/fnac-logo.png" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/pad-logo1.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/pad-logo.png" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="javascript:;">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/Megabrands-logo-hover.png" class="img-responsive" alt="">
                    <img src="<?php echo base_url()?>assets/pages/img/clients/Megabrands-logo.png" class="color-img img-responsive" alt="">
                </a>
            </div>


        </div>
    </div>
</div>
<!-- END CLIENTS -->
</div>
</div>



<?php include('footer.php');?>
