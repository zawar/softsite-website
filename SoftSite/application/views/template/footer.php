
<!-- BEGIN PRE-FOOTER -->
<div class="pre-footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN BOTTOM ABOUT BLOCK -->
            <div class="col-md-4 col-sm-4 pre-footer-col">
                <h2>About us</h2>
                <p class="page-font">SoftSite is a one stop shop for all your software needs, our team of highly experienced and competitive software enthusiasts has helped many reputed companies in modernizing their management systems. We design and develop software which do not only cater to organizations’ growing business needs but we also help them in enhancing their business potential.</p>
            </div>
            <!-- END BOTTOM ABOUT BLOCK -->

            <!-- BEGIN BOTTOM CONTACTS -->
            <div class="col-md-4 col-sm-4 pre-footer-col">
                <h2>Our Contacts</h2>
                <address class="margin-bottom-40 page-font">
                    Opposite Gaddafi Stadium<br>
                    Lahore, Pakistan<br>
                    Phone: 042 35750295<br>
                    Fax: 300 323 1456<br>
                    Email: <a href="mailto:info@metronic.com">info@softsite.pk</a><br>
                    Skype: <a href="skype:metronic">XYZ</a>
                </address>

            </div>
            <!-- END BOTTOM CONTACTS -->
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-4 col-sm-4 padding-top-10">
                2015 © Keenthemes. ALL Rights Reserved. <a href="javascript:;">Privacy Policy</a> | <a href="javascript:;">Terms of Service</a>
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN PAYMENTS -->
            <div class="col-md-4 col-sm-4">
                <ul class="social-footer list-unstyled list-inline pull-right">
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-skype"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-github"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dropbox"></i></a></li>
                </ul>
            </div>
            <!-- END PAYMENTS -->
            <!-- BEGIN POWERED -->
            <div class="col-md-4 col-sm-4 text-right">
                <p class="powered">Powered by: <a href="http://www.keenthemes.com/">KeenThemes.com</a></p>
            </div>
            <!-- END POWERED -->
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/plugins/respond.min.js');?>"></script>

<script src="<?php echo base_url()?>assets/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/corporate/scripts/back-to-top.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<!--<script src="<?php /*echo base_url('assets/plugins/fancybox/source/jquery.fancybox.pack.js');*/?>" type="text/javascript"></script>--><!-- pop up -->
<script src="<?php echo base_url()?>assets/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/plugins/owl.carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

<script src="<?php echo base_url()?>assets/corporate/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/pages/scripts/bs-carousel.js" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        Layout.initTwitter();
        //Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
        //Layout.initNavScrolling();
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>