<!DOCTYPE html>
<!--
Template: Metronic Frontend Freebie - Responsive HTML Template Based On Twitter Bootstrap 3.3.4
Version: 1.0.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase Premium Metronic Admin Theme: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <title>Portfolio 4 Column | Metronic Frontend</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <link rel="shortcut icon" href="favicon.ico">

    <!-- Fonts START -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="<?php echo base_url()?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Global styles END -->

    <!-- Page level plugin styles START -->
    <link href="<?php echo base_url()?>assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <!-- Page level plugin styles END -->

    <!-- Theme styles START -->
    <link href="<?php echo base_url()?>assets/pages/css/components.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/corporate/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/pages/css/portfolio.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/corporate/css/style-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/corporate/css/themes/red.css" rel="stylesheet" id="style-color">
    <link href="<?php echo base_url()?>assets/corporate/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/softsite-style.css" rel="stylesheet">
    <!-- Theme styles END -->
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
<!-- BEGIN STYLE CUSTOMIZER -->

<!-- END BEGIN STYLE CUSTOMIZER -->

<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-4 col-sm-4  additional-shop-info">
            </div>
            <div class="col-md-8 col-sm-8  additional-shop-info">
                <ul class="list-unstyled list-inline">
                    <li>
                        <i class="fa fa-phone icon-color"></i>
                        <span class="preheader-span-text-color">+92-42 35750295</span>
                    </li>

                    <li>
                        <i class="fa fa-envelope-o icon-color"></i>
                        <span class="preheader-span-text-color">info@softsite.com</span>
                    </li>
                    <button class="btn btn-danger hidden-md hidden-lg quote-btn">Get Free Quote</button>
                </ul>
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <div class="col-md-6">
            <a class="site-logo" href="index.html"><img src="<?php echo base_url()?>assets/corporate/img/logos/logo-medium.png" alt="Metronic FrontEnd"></a>
        </div>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <!-- BEGIN NAVIGATION -->

        <div class="header-navigation  font-transform-inherit">
            <ul>
                <li>
                    <a href="<?php echo base_url()?>index.php/Main/index">Home</a>
                </li>
                <li >
                    <a href="<?php echo base_url()?>index.php/Main/portfolio">Portfolio</a>

                </li>
                <li><a href="#">About Us</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">Services</a>

                    <ul class="dropdown-menu">
                        <li><a href="portfolio-4.html">PHP Developement</a></li>
                        <li><a href="portfolio-3.html">Magento</a></li>
                        <li><a href="portfolio-2.html">Visual Basic .NET</a></li>
                        <li><a href="portfolio-item.html">Parcel At Door</a></li>
                        <li><a href="portfolio-item.html">Graphics Designing</a></li>
                        <li><a href="portfolio-item.html">Animation</a></li>
                    </ul>
                </li>

                <li><a href="#">Contact us</a></li>
            </ul>
        </div>
        <!-- END NAVIGATION -->
        <button class="btn btn-danger hidden-sm hidden-xs hidden-md quote-btn">Get Free Quote</button>
    </div>

</div>

<!-- Header END -->